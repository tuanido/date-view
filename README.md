# date-view
Polymer element to format dates into a label.


## Usage


1. Import Web Components' polyfill:

    ```html
    <script src="bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>
    ```

2. Import Custom Element:

    ```html
    <link rel="import" href="src/date-view.html">
    ```

3. Start using it!

    ```html
    <date-view date="1454003380058" format="d/m/Y H:i:s"></date-view>
    ```

## Format options:

pattern |Meaning |example
--- |--- |---
`d` |Day of the month, 2 digits with leading zeros |01 to 31
`D` |A textual representation of a day, three letters |Mon through Sun
`w` |Numeric representation of the day of the week |0 (for Sunday) through 6 (for Saturday)
`m` |Numeric representation of a month, with leading zeros |01 through 12
`M` |A short textual representation of a month, three letters |Jan through Dec
`Y` |A full numeric representation of a year, 4 digits |Examples: 1999 or 2003
`y` |A two digit representation of a year |Examples: 99 or 03
`a` |Lowercase Ante meridiem and Post meridiem |am or pm
`A` |Uppercase Ante meridiem and Post meridiem |AM or PM
`g` |12-hour format of an hour without leading zeros |1 through 12
`G` |24-hour format of an hour without leading zeros |0 through 23
`h` |12-hour format of an hour with leading zeros |01 through 12
`H` |24-hour format of an hour with leading zeros |00 through 23
`i` |Minutes with leading zeros |00 to 59
`s` |Seconds, with leading zeros |00 through 59
`O` |Difference to Greenwich time (GMT) in hours |Example: +0200
`P` |Difference to Greenwich time (GMT) with colon between hours and minutes (added in PHP 5.1.3) |Example: +02:00
`T` |Timezone setting of this machine |Examples: EST, MDT …
`Z` |Timezone offset in seconds. The offset for timezones west of UTC is always negative, and for those east of UTC is always positive. |-43200 through 43200
`c` |ISO 8601 date |2004-02-12T15:19:21+00:00
`r` |RFC 2822 formatted date |Example: Thu, 21 Dec 2000 16:01:07 +0200
`U` |Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)